import styles from './Dashboard.module.scss'
import {useCallback, useState} from "react";
import classNames from "classnames";
import Dropdown from "../uploads/dropdown.module";
import Image from 'next/image'
import w1 from '../../public/img/1.png';
import w11 from '../../public/img/1-1.png';
import w2 from '../../public/img/2.png';
import w21 from '../../public/img/2-2.png';
import weather from '../../public/img/w.png';
import count from '../../public/img/count.png';
import circle from '../../public/img/circle.png';


const model = (title, isSelect) => {
    return {
        title: title,
        isSelect: isSelect
    }
}

const dates = [
    {
        date: "27.05.2022",
    },

];

export default function DashboardModule() {
    const handleStateChange = useCallback((state) => console.log(state), []);
    const [times, setTimes] = useState([model("Сегодня", true), model('Вчера', false), model('Неделя', false), model('Месяц', false), model('Квартал', false), model('Год', false)])


    const onSelect = (index) => {
        let a = times;
        a = a.map((value) => {
            value.isSelect = false;
            return value;
        })
        a[index].isSelect = true;
        setTimes(a);
    }

    return (
        <div className={styles.main}>
            <div className={styles.title}>Аналитика</div>
            <div className={styles.wrapper}>
                <div className={styles.column1}>
                    <div className={styles.row}>
                        <div className={styles.times}>
                            {times.map((item, index) => {
                                return (<div key={`hello${index}`}
                                             className={classNames(styles.item, item.isSelect === true ? styles.selected : '')}
                                             onClick={() => onSelect(index)}>
                                    {item.title}
                                </div>)
                            })}
                        </div>
                        <Dropdown
                            className={styles.dates}
                            items={dates}
                            id="dates"
                            onStateChange={handleStateChange}
                            isCalendar={true}
                            placeholderLabel={'27.05.2022'}/>
                        <Dropdown
                            items={dates}
                            id="places"
                            onStateChange={handleStateChange}
                            placeholderLabel={'о. Матвеев'}/>
                    </div>
                    <div className={styles.img}>
                        <Image src={w1} width={527} height={326} alt={'image with walrus'}/>
                        <Image src={w11} width={527} height={326} alt={'image with walrus'}/>
                    </div>

                    <div className={styles.img}>
                        <Image src={w2} width={527} height={326} alt={'image with walrus'}/>
                        <Image src={w21} width={527} height={326} alt={'image with walrus'}/>
                    </div>
                </div>
                <div className={styles.column2}>
                    <Image src={weather} width={413} height={262} alt={'Weather'}/>
                    <Image src={count} width={412} height={265} alt={'Walrus count'}/>
                    <Image src={circle} width={410} height={382} alt={'Dash'}/>
                </div>
            </div>


        </div>

    )

}
