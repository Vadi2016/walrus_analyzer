import style from "./Main.module.scss";

export default function WrapperLayoutModule({children}) {
    return (
        <div className={style.wrapper}>
            {children}
        </div>
    )
}
