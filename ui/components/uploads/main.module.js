import style from "./Main.module.scss";
import WrapperLayoutModule from "./wrapper.layout.module";
import UploadModule from "./upload.module";
import {useReducer} from "react";

export default function MainModule() {
    // reducer function to handle state changes
    const reducer = (state, action) => {
        switch (action.type) {
            case "SET_IN_DROP_ZONE":
                return { ...state, inDropZone: action.inDropZone };
            case "ADD_FILE_TO_LIST":
                return { ...state, fileList: state.fileList.concat(action.files) };
            default:
                return state;
        }
    };

    // destructuring state and dispatch, initializing fileList to empty array
    const [data, dispatch] = useReducer(reducer, {
        inDropZone: false,
        fileList: [],
    });


    return (
        <div className={style.main}>
            <WrapperLayoutModule>
                <UploadModule data={data} dispatch={dispatch}/>
            </WrapperLayoutModule>
        </div>
    )
}
