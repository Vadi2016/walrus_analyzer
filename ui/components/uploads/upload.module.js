import style from "./Upload.module.scss";
import FilePreviewModule from "./file.preview.module";
import Dropdown from "./dropdown.module";
import {useCallback} from "react";
import {useRouter} from "next/router";

const items = [
    {
        label: "о. Матвеев",
        value: "january"
    },

];

export default function UploadModule ({ data, dispatch }) {
    const handleStateChange = useCallback((state) => console.log(state), []);
    const router = useRouter();

    const handleDragEnter = (e) => {
        e.preventDefault();
        e.stopPropagation();
        dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: true });
    };

    // onDragLeave sets inDropZone to false
    const handleDragLeave = (e) => {
        e.preventDefault();
        e.stopPropagation();

        dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: false });
    };

    // onDragOver sets inDropZone to true
    const handleDragOver = (e) => {
        e.preventDefault();
        e.stopPropagation();

        // set dropEffect to copy i.e copy of the source item
        e.dataTransfer.dropEffect = "copy";
        dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: true });
    };

    // onDrop sets inDropZone to false and adds files to fileList
    const handleDrop = async (e) => {
        e.preventDefault();
        e.stopPropagation();

        // get files from event on the dataTransfer object as an array
        let files = [...e.dataTransfer.files];
        // ensure a file or files are selected
        if (files && files.length > 0) {
            // loop over existing files
            const existingFiles = data.fileList.map((f) => f.name);
            // check if file already exists, if so, don't add to fileList
            // this is to prevent duplicates
            files = files.filter((f) => !existingFiles.includes(f.name));

            // dispatch action to add selected file or files to fileList
            dispatch({ type: "ADD_FILE_TO_LIST", files });
        }

        await uploadFiles(files);
    };

    // handle file selection via input element
    const handleFileSelect = async (e) => {
        // get files from event on the input element as an array
        let files = [...e.target.files];

        // ensure a file or files are dropped
        if (files && files.length > 0) {
            // loop over existing files
            const existingFiles = data.fileList.map((f) => f.name);
            // check if file already exists, if so, don't add to fileList
            // this is to prevent duplicates
            files = files.filter((f) => !existingFiles.includes(f.name));

            // dispatch action to add droped file or files to fileList
            dispatch({ type: "ADD_FILE_TO_LIST", files });
            // reset inDropZone to false
            dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: false });
        }
        await uploadFiles(files);
    };

    // to handle file uploads
    const uploadFiles = async (files) => {
        // get the files from the fileList as an array
        // initialize formData object
        const formData = new FormData();
        // loop over files and add to formData
        files.forEach((file) => formData.append("files", file));

        // Upload the files as a POST request to the server using fetch
        // Note: /api/fileupload is not a real endpoint, it is just an example
        const response = await fetch("/api/fileupload", {
            method: "POST",
            body: formData,
        });

        //successful file upload
        if (response.ok) {
            console.log("Files uploaded successfully");
        } else {
            // unsuccessful file upload
            alert("Error uploading files");
        }
    };

    return (
        <div className={style.main}>
            <div className={style.title}>
                Добавление данных
            </div>
            <div className={style.upload}
                 onDrop={(e) => handleDrop(e)}
                 onDragOver={(e) => handleDragOver(e)}
                 onDragEnter={(e) => handleDragEnter(e)}
                 onDragLeave={(e) => handleDragLeave(e)}>
                <div className={style.inner}>
                    <svg width="56" height="55" viewBox="0 0 56 55" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clipPath="url(#clip0_40_1386)">
                            <path d="M50.8169 33.0256V47.7087C50.8169 48.8736 49.8692 49.8213 48.7042 49.8213H7.29579C6.13086 49.8213 5.18312 48.8736 5.18312 47.7087V33.0256H0.957764V47.7087C0.957764 51.2035 3.801 54.0467 7.29579 54.0467H48.7042C52.199 54.0467 55.0423 51.2035 55.0423 47.7087V33.0256H50.8169Z" fill="#596370"/>
                            <path d="M28.0002 0.0361938L14.8716 13.1648L17.8593 16.1525L25.8875 8.12436V41.0521H30.1128V8.12436L38.141 16.1525L41.1288 13.1648L28.0002 0.0361938Z" fill="#596370"/>
                        </g>
                        <defs>
                            <clipPath id="clip0_40_1386">
                                <rect width="54.0845" height="54.0845" fill="white" transform="translate(0.957764)"/>
                            </clipPath>
                        </defs>
                    </svg>
                    <input
                        id="fileSelect"
                        type="file"
                        multiple
                        onChange={(e) => handleFileSelect(e)}
                        className={style.files}
                    />
                    <div className={style.text}>
                        Загрузите или перетащите фотографии
                    </div>
                </div>
            </div>
            <FilePreviewModule fileData={data}/>
            <div className={style.caption}>Выберите лежбище</div>
            <Dropdown
                items={ items }
                id="month-switcher"
                onStateChange={ handleStateChange }
                placeholderLabel={'Выберите лежище'}/>
            <button className={style.btn} onClick={() => {router.push( '/analytics')}}>
                Применить
            </button>
        </div>
    )
}
