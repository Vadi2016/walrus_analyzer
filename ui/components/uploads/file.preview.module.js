import style from "./FilePreview.module.scss";
import Image from 'next/image'
import {useRef} from "react";


export default function FilePreviewModule({fileData}) {
    const lazyRoot = useRef(null)

    return (
        <div className={style.fileList}>
            <div className={style.fileContainer}>
                {/* loop over the fileData */}
                <ul className={style.fileList}>
                    {fileData.fileList.map((f) => {
                        return (
                            <li key={f.name}>
                                <div
                                    ref={lazyRoot}
                                    key={f.name}
                                    className={style.fileName}>
                                    <Image
                                        className={style.img}
                                        lazyRoot={lazyRoot}
                                        src={`/dataset/${f.name}`}
                                        alt="Picture of the author"
                                        width={152}
                                        height={83}
                                    />
                                </div>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
}
