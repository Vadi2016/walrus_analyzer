import React, { useState, useContext} from 'react';

const ServiceContext = React.createContext(
    {
        isShowModal: false
    }
);

export function usesServiceContext() {
    return useContext(ServiceContext);
}

export function ServiceProvider({ children }) {
    const [value, setValue] = useState(false);

    return (
        <ServiceContext.Provider value={{value, setValue}}>
            {children}
        </ServiceContext.Provider>
    );
}
