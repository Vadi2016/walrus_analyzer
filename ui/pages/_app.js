import '../styles/globals.css'
import {ServiceProvider} from "../context/state";

function WalrusAnalytics({Component, pageProps}) {
    return (
        <ServiceProvider>
            <Component {...pageProps} />
        </ServiceProvider>
    )
}

export default WalrusAnalytics
