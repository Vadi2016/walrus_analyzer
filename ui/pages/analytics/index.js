import MainLayout from "../../components/MainLayout";
import DashboardModule from "../../components/dashboard/dashboard.module";

export default function AnalyticsDashboard() {
    return (
        <MainLayout>
            <DashboardModule/>
        </MainLayout>

    )
}
