import MainLayout from "../components/MainLayout";
import MainModule from "../components/uploads/main.module";

export default function Home() {
    return (
        <MainLayout>
            <MainModule/>
        </MainLayout>

    )
}
