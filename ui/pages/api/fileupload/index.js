import { IncomingForm } from 'formidable'
import mv from 'mv';


export const config = {
    api: {
        bodyParser: false,
    }
};

export default async (req, res) => {
    await new Promise((resolve, reject) => {
        const form = new IncomingForm()

        form.parse(req, (err, fields, files) => {
            if (err) return reject(err)
            console.log(fields, files)
            console.log(files.files.filepath)
            let oldPath = files.files.filepath;
            let newPath = `./public/dataset/${files.files.originalFilename}`;
            mv(oldPath, newPath, function(err) {
            });
            res.status(200).json({ fields, files })
        })
    });
}
